# v0.0.14

## Fixed
* `compute_viewfactor` function : a fixed decimal truncature was yielding wrong results, removed. 
* `compute_viewfactor` function : the "virtual displacement direction. It was done according to the adjacent cells normal, now according to the cell centroids direction. 

## Added 
* Some coments to the code, new and translations (from franch to english),
* Precision in the documentation.

# v0.0.12 (27.1.2023)

## Fixed
* Dependency issue with `pandas`. Will now work with `pandas >= 1.5.0`

## Added
* new precision parameter for the `compute_viewfactor` function : `epsilon` to get the desired precision. 
* New example in the documentation : Thermal Comfort MRT with Pyvista's Doorman.

# v0.0.11 (19.8.2022)

## Added 
* use of `numba` library in the `integrand` and `commpute_viewfactor` functions. 
* Documentation accessible at https://arep-dev.gitlab.io/pyViewFactor/ 

# v0.0.10 (8.8.2022)

## Added
* `__init__.py` and `_version.py`

# v0.0.8 (8.8.2022)

## Fixed (1 change)
* `README.md` modifications for images absolute URL (for Pypi)

# v0.0.7 (8.8.2022)

## Fixed (5 changes)
* Remove debugging prints throughout the code
* Update examples
* Typo in authors names
* Update obstruction function (typo)
* Update `get_visibility_raytrace` function call (typo)
* Update `README.md`
