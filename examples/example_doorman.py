# -*- coding: utf-8 -*-
from tqdm import tqdm
import numpy as np
import pyvista as pv
import pyviewfactor as pvf

def fc_Fwall(nom_vtk):
    mesh=pv.read(nom_vtk)
    
    # find all types of walls
    wall_types=list(np.unique(mesh['names']))
    # remove the individual from the list (still named 'cylinder'...)
    wall_types.remove('cylinder\r') # with dirty trailing character
    # where is the doorman in the list?
    index_doorman=np.where(mesh['names']=='cylinder\r')[0]
    # prepare storage for the different walls in a dict
    dict_F={}
    # loop over wall types
    for type_wall in wall_types:
        # prepare for storing doorman to wall view factor
        F=np.zeros(mesh.n_cells)
        # get the indices of this type of wall
        indices=np.where(mesh['names']==type_wall )[0]
        # loop over
        for i in indices:
            wall=mesh.extract_cells(i)
            wall=pvf.fc_unstruc2poly(wall) # convert for normals
            # ... for each facet of the individual
            for idx in tqdm(index_doorman):
                face = mesh.extract_cells(idx)
                face =pvf.fc_unstruc2poly(face) # convert for normals
                # check if faces can "see" each other
                if pvf.get_visibility(wall,face):
                    Ffp=pvf.compute_viewfactor(wall,face) # compute face2wall view factor
                else:
                    Ffp=0
                F[idx]=Ffp
        #store array F in e.g. dict_F['F_ceiling']
        dict_F['F_'+type_wall.replace('\r','')]=F
    return dict_F

# download it directly from https://gitlab.com/arep-dev/pyViewFactor/-/blob/main/examples/example_doorman.vtk ...
# ...or get it from this repository's examples
file="./example_doorman.vtk"
# compute the view factors or the doorman to the different wall types in the scene
dict_F=fc_Fwall(file)

# re-read and store
mesh=pv.read(file)
# loop over what is in the dictionary of view factors
for elt in dict_F.keys():
    mesh[elt.replace('\r','')]=dict_F[elt] # name the field
mesh.save(file) # store in the intial VTK

# have a look without paraview with fancy colors
mesh.plot(cmap='jet',lighting=False)
